package com.edgardribeiro.javafxsts.controller;

import org.springframework.stereotype.Component;

import javafx.application.HostServices;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

@Component
public class SimpleUiController {
	
	private final HostServices hostServices;
	
	@FXML
	public Label label;
	
	@FXML
	public Button button;

	public SimpleUiController(HostServices hostServices) {
		this.hostServices = hostServices;
	}
	
	public void initialize() {
		this.button.setOnAction(ActionEvent -> this.label.setText(this.hostServices.getDocumentBase()));
	}

}
